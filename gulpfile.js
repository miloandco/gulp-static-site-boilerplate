'use strict';

var gulp 		= require('gulp'),
	htmlrender 	= require('gulp-htmlrender'),
	sass 		= require('gulp-sass'),
	sourcemaps 	= require('gulp-sourcemaps'),
	imagemin 	= require('gulp-imagemin'),
	jshint 		= require('gulp-jshint'),
	concat 		= require('gulp-concat'),
	rename 		= require('gulp-rename'),
	debug 		= require('gulp-debug'),
	browserSync = require('browser-sync').create();



/*=============================================
=                   Paths                     =
=============================================*/

var basePaths = {
    src: 			'src/assets/',
    dest: 			'public/assets/',
    npm: 			'node_modules/'
};

var paths = {
    html: {
    	partials: 'src/_partials/',
    	src: 'src/*.html',
    	srcAll: 'src/**/*.html',
    	dest: 'public/'
    },
    styles: {
        src: 		basePaths.src + 'sass/**/*.scss',
        dest: 		basePaths.dest + 'css/'
    },
    scripts: {
        src: 		basePaths.src + 'js/**/*.js',
        dest: 		basePaths.dest + 'js/'
    },
    images: {
        src: 		basePaths.src + 'images/**/*.*',
        dest: 		basePaths.dest + 'images/'
    }
};



/*=============================================
=                   Tasks                     =
=============================================*/

gulp.task('render', function() {
	return gulp.src(paths.html.src, {read: false})
		.pipe(debug({title: 'Files found:'}))
		.pipe(htmlrender.render())
		.pipe(gulp.dest( paths.html.dest ));
});

gulp.task('sass', function () {
	return gulp.src( paths.styles.src )
		.pipe(debug({title: 'Files found:'}))
		.pipe(sourcemaps.init())
		.pipe(sass().on('error', sass.logError))
		.pipe(sourcemaps.write())

		.pipe(gulp.dest( paths.styles.dest ))
		.pipe(browserSync.stream());
});

gulp.task('scripts', function () {
	return gulp.src([
		//'node_modules/bootstrap-sass/assets/javascripts/bootstrap/affix.js', <- This is how you concat bootstrap js modules
		'src/assets/js/main.js', 
	])
    .pipe(concat('all.js'))
    .pipe(gulp.dest( paths.scripts.dest ));
});

gulp.task('images', function () {
	return gulp.src( paths.images.src )
		.pipe(debug({title: 'Files found:'}))
		.pipe(imagemin())
		.pipe(gulp.dest( paths.images.dest ));
});

gulp.task('serve', ['sass'], function() {
    browserSync.init({
        server: "./public"
    });
});



/*=============================================
=                   Watchers                  =
=============================================*/

gulp.task('watch', function() {
    gulp.watch([paths.html.srcAll], ['render']).on('change', browserSync.reload);
    gulp.watch(paths.styles.src, ['sass']).on('change', browserSync.reload);
    gulp.watch(paths.scripts.src, ['scripts']).on('change', browserSync.reload);
    gulp.watch(paths.images.src, ['images']).on('change', browserSync.reload);

    gulp.start('serve');
});



/*=============================================
=                  Default                    =
=============================================*/
 
gulp.task('default', ['render', 'sass', 'scripts', 'images']);

