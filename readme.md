## Boilerplate to kickstart a static website project using gulp.

## Using the follow gulp-packages

* gulp
* gulp-htmlrender
* gulp-sass
* gulp-sourcemaps
* gulp-imagemin
* gulp-jshint
* gulp-concat
* gulp-rename
* gulp-debug
* browser-sync

## Contributing

Feel free to contribute.

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

### License

Feel free to use under the [MIT license](http://opensource.org/licenses/MIT)
